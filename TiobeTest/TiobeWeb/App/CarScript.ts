﻿interface Car {
    Brand: string;
    Type: string;
}

function giveBrand(car: Car) {
    return "Brand: " + car.Brand + " Type: " + car.Type; 
}

var car = { Brand: "Mercedes", Type: "sl280" };