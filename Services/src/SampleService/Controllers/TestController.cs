﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Framework.Logging;
using Common;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SampleService.Controllers
{
    [Route("api/[controller]")]
    public class TestController : Controller
    {
        private ILogger _logger;
        private SampleRepository _repos;

        public TestController(ILoggerFactory loggerFactory, SampleRepository repos)
        {
            _logger = loggerFactory.CreateLogger<TestController>();
            _repos = repos;
        }

        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            Context.Response.StatusCode = 200;
            _repos.OpenCloseConnection();

            return new ObjectResult(true);
        }
    }
}
